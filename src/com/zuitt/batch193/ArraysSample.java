package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {

    public static void main(String[] args) {
        //Arrays = list of things. Limited in Java

        //Array declaration/ Instantiation
        int[] intArray = new int[3];
        //the int[] - indicate that this is an int data type and can hold multiple int values
        //"new" keyword is used to tell Java to create the identifier
        //int[3] indicates the amount of integer values in the array
        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;
        System.out.println(intArray[2]);

        //square bracket = other way to declare an array
        int intSample[] = new int[3];
        intSample[0] = 50;
        System.out.println(intSample[0]);

        //String array
        String stringArray[] = new String[3];
        stringArray[0] = "Loid";
        stringArray[1] = "Yor";
        stringArray[2] = "Anya";
        System.out.println(stringArray[0]);

        //Declaration with initialization based on number of elements
        int[] intArray2 = {100, 200, 300, 400, 500};
        System.out.println(intArray2[2]);
        System.out.println(intArray2); //prints memory allocation of the array
        //to get the actual value of array
        System.out.println("to string");
        System.out.println(Arrays.toString(intArray2));
        //"Arrays" is a class that contains various methods for manipulating arrays
        //.toString is a method that returns a string representation of the contents of the specified array

        //methods used in arrays
        //sort
        Arrays.sort(stringArray);

        System.out.println(Arrays.toString(stringArray));

        //binary search; commonly used for string, must be sorted first
        String searchTerm = "Anya";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);
        //binary search will return an index if the match is found
        //if the searchTerm is not found in the array, the output is displayed as a negative value


        //Multidimensional array
        //two dimensional array, can be described by two lengths nested on each other, like a matrix
        String[][] classroom = new String[3][3];
        //first row
        //[0][1] first row, second element
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Naeyon";
        //Second row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanji";
        //third
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));
        //deepToSting() returns a string of the nested contents of the array

        //ArrayLists = resizeable and can be updated
        ArrayList<String> students = new ArrayList<>();

        //to add elements to an arraylist
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //retrieving elements using get()
        System.out.println(students.get(0));

        //update or change the elements
        students.set(0, "George"); //first value = index, second value = updated value
        System.out.println(students);

        //delete/ remove element
        students.remove(1); //index number
        System.out.println(students);

        //removing all
        students.clear(); //clear all elements
        System.out.println(students);

        //get length of arraylist
        System.out.println(students.size());

        //Arraylist with initial value
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);


        //HashMaps
        //are stored by specifying the data type of the key and of the value
        //flexible object pattern
        //HashMap<fieldDataType, valueDataType>
        HashMap<String, String> employeeRole = new HashMap<>();

        //adding elements to hashmaps
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");
        System.out.println(employeeRole);

        //retrieve field value; using fieldDataType
        System.out.println(employeeRole.get("Captain"));

        //remove fields
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        //getting the keys of the elements of HashMap by using the keySet()
        System.out.println(employeeRole.keySet());

        //HashMaps with int as values
        HashMap<String, Integer> grades = new HashMap<>();
        grades.put("Joe", 89);
        grades.put("John", 93);
        System.out.println(grades);

        //Hashmap with arraylist
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(80, 75, 90));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(86, 87, 96));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("John", gradesListB);

        System.out.println(subjectGrades);
        //getting an index if ArrayList is used in hashmap
        System.out.println(subjectGrades.get("Joe").get(2));


    }
}
