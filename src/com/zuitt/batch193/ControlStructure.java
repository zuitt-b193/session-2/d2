package com.zuitt.batch193;

public class ControlStructure {

    public static void main(String[] args) {

        //Operators
        //Types of operators:
        //1. Arithmetic => +, -, *, /, %
        //2. Comparison => >, <, ==, ===, !=, >=, <=
        //3 Logical => &, |, !, ||, &&, ? :
        //4. Assignment => =

        //Control Structures
        int num1 = 10;
        int num2 = 20;
//        if (num1 > 5) {
//            System.out.println("Num1 is greater than 5");
//        }
        if (num1 > 50)
            System.out.println("Num1 is greater than 5");
        else
            System.out.println("Not");

        if (num2 == 100)
            System.out.println("Num2 is equal to 100");
        else if (num2 == 50)
            System.out.println("Num2 is greater than 49");
        else
            System.out.println("Anything");

        //& and | when used as operators evaluates both sides
        //&& and || ("short-circuit" operators) when used as operators, will not evaluate the right side anymore if the left side is false already
            //false && ...
        //short circuiting is a technique applicable only to the AND and OR operators wherein if-statements or other control structures can exit early by ensuring safety of operation or efficiency

        int x = 15;
        int y = 0;
        if (y > 5 && x/y == 0)
            System.out.println(" Result is: " + x/y);
        else
            System.out.println("Condition has short circuited");


        //Switch statements
        //Direction (North, East, West, South)

        int directionValue = 3;

        switch (directionValue){
            case 1:  //a case block within a switch represents a single case or a single possible value for the statement
                System.out.println("North");
                break; //"break" keyword tells that this specific case block has finished
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:  //handles the scenario if there are no cases that were satisfied
                System.out.println("Invalid direction");


        }


    }
}
